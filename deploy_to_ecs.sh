#!/usr/bin/env bash
set -e
deploy_to_ecs() {

  echo $1
  echo $2
  echo $3
  echo $4
  echo $5
  echo $6
  APP_CONTAINER_NAME=${5:-$1}
  deployment_type=${6:-normal}
  echo "$APP_CONTAINER_NAME"
  echo "$deployment_type"

  #Registering new revision of task-definition with new image name
  LATEST_TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition $1  --region us-east-1)
  echo "Latest Task Definition:"
  echo $LATEST_TASK_DEFINITION
  PREVIOUS_IMAGE_NAME=$(echo $LATEST_TASK_DEFINITION \
    | jq -r --arg APP_CONTAINER_NAME "$APP_CONTAINER_NAME" '.taskDefinition.containerDefinitions[0] | select(.name==$APP_CONTAINER_NAME) | .image')
  echo "Previous Image Name:"
  echo $PREVIOUS_IMAGE_NAME
  if [ $PREVIOUS_IMAGE_NAME != $2 ]
  then 
    echo $LATEST_TASK_DEFINITION \
        | jq '.taskDefinition' \
        | jq 'del(.status)' | jq 'del(.compatibilities)'| jq 'del(.taskDefinitionArn)'| jq 'del(.requiresAttributes)'| jq 'del(.revision)' \
        | jq -r --arg APP_CONTAINER_NAME "$APP_CONTAINER_NAME" '(.containerDefinitions[0] | select(.name==$APP_CONTAINER_NAME) | .image)='\"$2\" \
        > /tmp/tmp-$1.json
    DEFINITION_OUTPUT=$(aws ecs register-task-definition --family $1 --region us-east-1 --cli-input-json file:///tmp/tmp-$1.json)
    echo $DEFINITION_OUTPUT
  fi 

  #Updating service with newest revison of task-definition
  if [ $deployment_type = "one_instance" ]
    then
      echo "deploying only one instant"
      DEPLOYMENT_OUTPUT=$(aws ecs update-service --cluster $3 --region us-east-1 --service $4 --task-definition $1 --desired-count 1)
    else
      DEPLOYMENT_OUTPUT=$(aws ecs update-service --cluster $3 --region us-east-1 --service $4 --task-definition $1)
  fi

  #Checking for Deployment Status to be "ACTIVE"
  counter=1
  while [ $counter -le 120 ]
  do
      sleep 15
      UI_SERVICE=$(aws ecs describe-services --cluster $3 --services $4 --region us-east-1)
      RUNNING=$(echo $UI_SERVICE | jq '.services[0].deployments[0].runningCount')
      DESIRED=$(echo $UI_SERVICE | jq '.services[0].deployments[0].desiredCount')
      LAST_EVENT=$(echo $UI_SERVICE | jq '.services[0].events[0].message')
      echo "Last event logging: $LAST_EVENT"
      echo "Running Count: $RUNNING"
      echo "Desired Count: $DESIRED"
      if [ $RUNNING = $DESIRED ]
      then
          echo "Deployment Successful"
          if [[ $LAST_EVENT =~ "has reached a steady state" ]]; then
            echo "100% Deployment Successful";
          else
            echo "counts equal does not mean success";
          fi
          return 0
      fi
      ((counter++))
  done
  #Failed
  return 1
}

deploy_to_ecs $1 $2 $3 $4 $5 $6
